function initMap(savedLatLng) {
    if ($('#map').length > 0) {
        latLng = {lat: -28.024, lng: 140.887};
        if ((slat = $('#company-latitude').val()) && (slng = $('#company-longitude').val())) {
            latLng = new google.maps.LatLng(slat,slng);
        }
        console.log(latLng);
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: latLng
        });

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: latLng
        });
        marker.addListener('click', toggleBounce);
        marker.addListener('dragend', handleDragEnd);
    } else if ($('#map-all').length > 0) {
        var map = new google.maps.Map(document.getElementById('map-all'), {
            zoom: 2,
            center: new google.maps.LatLng(50, 50)
        });
        var markers = companies.map(function(location, i) {
            return new google.maps.Marker({
                position: new google.maps.LatLng(location.lat, location.lng),
                label: location.name
            });
        });
        // $.each(companies, function(key, value){
        //     marker = new google.maps.Marker({
        //         map: map,
        //         draggable: false,
        //         animation: google.maps.Animation.DROP,
        //         position: new google.maps.LatLng(value.lat, value.lng)
        //     });
        // });
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }
}
function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}
function handleDragEnd(data) {
    $('#company-latitude').val(data.latLng.lat);
    $('#company-longitude').val(data.latLng.lng);
}