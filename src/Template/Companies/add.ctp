<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="companies form large-9 medium-8 columns content">
    <?= $this->Form->create($company, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Add Company') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('city');
            echo $this->Form->file('logo');
            echo $this->Form->control('established_date');
        ?>
        <div id="map" style="height: 300px;"></div>
        <div class="hidden">
            <?php
            echo $this->Form->control('latitude', ['id' => 'company-latitude']);
            echo $this->Form->control('longitude', ['id' => 'company-longitude']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
