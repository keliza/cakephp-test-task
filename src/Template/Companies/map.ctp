<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Company[]|\Cake\Collection\CollectionInterface $companies
  */
?>
<div id="map-all" style="min-height: 500px">

</div>

<script>
    var companies = <?= json_encode($data);?>
</script>
