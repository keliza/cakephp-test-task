<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $company->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Companies'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="companies form large-9 medium-8 columns content">

    <?= $this->Form->create($company, ['enctype' => 'multipart/form-data']) ?>
    <fieldset>
        <legend><?= __('Edit Company') ?></legend>
        <?php if ($company->logo) :?>
            <div>
                <img src="<?= '/logos/' . $company->logo; ?>" width="100" style="clear: both;">
            </div>
        <?php endif;?>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('city');

            echo $this->Form->file('logo');
            echo $this->Form->control('established_date');
        ?>
        <div id="map" style="height: 300px;"></div>
        <div class="hidden">
            <?php
            echo $this->Form->control('latitude', ['id' => 'company-latitude']);
            echo $this->Form->control('longitude', ['id' => 'company-longitude']);
            ?>
        </div>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
